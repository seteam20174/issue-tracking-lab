Program Project1;

Type
    Massive = array[1..1000] of longint;

Const
     TreeHundredMb = 307200;
     MaxNumberofFiles = 1000;
     FrequencyRate = 1024;

Procedure GenerateNumberofFiles(var n: integer);
Begin
     n := random(MaxNumberofFiles);
     WriteLn(n);
End;

Procedure GenerateMassofSizes(var DefaultMass: Massive; n:integer);
var
    i: integer;
Begin
     for i:=1 to n do
     Begin
         DefaultMass[i]:=random(TreeHundredMb);
         WriteLn(DefaultMass[i]);
     End;
End;

Procedure PrintSize(Element: longint);
var
    k,t: integer;
Begin
    k:=Element div FrequencyRate;
    t:=Element - (k*FrequencyRate);
        if k > 0 then
            Write(k,'M ');
        if t>0 then
            Write(t,'K ');
    WriteLn;
End;

Procedure SearchMaxSize(DefaultMass: Massive; n:integer);
var
    i: integer;
    Max : longint;
Begin
    WriteLn('-----------------------------------');
    Max := DefaultMass[1];
    for i:= 2 to n do
    Begin
        if DefaultMass[i] > Max then
            Max := DefaultMass[i];
    End;
    Write('MaxElemtnt = ');
    PrintSize(Max);
End;

Procedure SearchMinSize(DefaultMass: Massive; n:integer);
var
    i: integer;
    Min : longint;
Begin
    WriteLn('-----------------------------------');
    Min := DefaultMass[1];
    for i:= 2 to n do
    Begin
        if DefaultMass[i] < Min then
            Min := DefaultMass[i];
    End;
    Write('MinElement = ');
    PrintSize(Min);
End;

Procedure SearchAverage(DefaultMass: Massive; n:integer);
var
    i: integer;
    Sum: longint;
Begin
    WriteLn('-----------------------------------');
    for i:= 1 to n do
    Begin
        Sum := Sum + DefaultMass[i];
    End;
    Write('Average = ');
    PrintSize(round(Sum/n));
End;

Procedure CheckMaximumAvaliablePlace(var DefaultMAP: string; var DefaultKilobytes: longint);
var
    lngth, count, i, err: byte;
    check: boolean;
    temp: longint;
Begin
    check := false;
    count := 0;
    while check <> true do
    Begin
        lngth := length(DefaultMAP);
        if  (DefaultMAP[1] <> '0') and  ((DefaultMAP[lngth] = 'M') or  (DefaultMAP[lngth] = 'G')) then
          count := count + 2;
       if lngth > 2 then
       Begin
           for i := 2 to lngth - 1 do
           Begin
               if  (ord(DefaultMAP[i]) >= 48) and  (ord(DefaultMAP[i]) <= 57) then
                  inc(count);
           End;
       End;
       if count = lngth then
         check := true
       else
       Begin
         count := 0;
         WriteLn('Error!Try again.');
         ReadLn(DefaultMAP);
       End;
    End;
    if DefaultMAP[lngth] = 'M' then
    Begin
        delete(DefaultMAP, lngth, 1);
        val(DefaultMAP, temp, err);
        DefaultKilobytes := temp * FrequencyRate;
    End;
    if DefaultMAP[lngth] = 'G' then
    Begin
        delete(DefaultMAP, lngth, 1);
        val(DefaultMAP, temp, err);
        DefaultKilobytes := temp * FrequencyRate * FrequencyRate;
    End;
End;

Procedure CompareMAPAndSumOfSizes(var DefaultAnswer: boolean; DefaultKilobytes: longint; DefaultMass: Massive; n: integer);
var
    i: byte;
    Sum: longint;
Begin
    Sum := 0;
    for i := 1 to n do
         Sum := Sum + DefaultMass[i];
    PrintSize(Sum);
    if DefaultKilobytes >= Sum then
         DefaultAnswer := false
    else DefaultAnswer := true;
End;

var
     answer: boolean;
     k,number: integer;
     Kilobytes: longint;
     MassofSizes: Massive;
     MAP: string;
Begin
     Randomize;
     GenerateNumberofFiles(number);
     GenerateMassofSizes(MassofSizes, number);
     for k:=1 to number do
     Begin
         PrintSize(MassofSizes[k]);
     End;
     SearchMaxSize(MassofSizes, number);
     SearchMinSize(MassofSizes, number);
     SearchAverage(MassofSizes, number);
     WriteLn;
     WriteLn('Enter maximum avaliable place:');
     Read(MAP);
     CheckMaximumAvaliablePlace(MAP, Kilobytes);
     CompareMAPAndSumOfSizes(answer, Kilobytes, MassofSizes, number);
     if answer = true then
        WriteLn('Yes.')
     else WriteLn('No.');
     ReadLn;
End.